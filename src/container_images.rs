use rqlite_rs::{error::QueryBuilderError, query::RqliteQuery, FromRow, IntoTypedRows, RqliteClient};
use serde::Serialize;
use utoipa::ToSchema;

use crate::{container_images_logs::{ContainerImagesLogs, InvalidStatusError, RawContainerImagesLogs}, executors::{self, ExecutorError}, ItemCount, ModelError};

#[derive(FromRow, Clone, ToSchema, Serialize)]
pub struct ContainerImage {
    pub id: String,
    pub name: String,
    pub tag: String,
}

#[derive(FromRow, Clone, ToSchema, Serialize)]
pub struct PaginatedContainerImage {
    pub id: String,
    pub name: String,
    pub tag: String,
    pub status: String,
}

/// placeholder struct for semantic reasons
pub struct ContainerImages;

#[derive(ToSchema, FromRow, Serialize, Clone)]
pub struct LogEntry {
    pub id: String,
    pub name: String,
    pub tag: String,
    pub status: String,
    pub timestamp_utc: String,
    pub comment: Option<String>,
}


impl ContainerImage {

    pub async fn insert_requested(
        conn: &RqliteClient,
        id: &str,
        name: &str,
        tag: &str
    ) -> Result<(), ModelError> {

        let insert_queries = ContainerImage::build_insert_requested_queries(
            id,
            name,
            tag,
        )?;

        executors::transaction(conn, insert_queries).await?;

        Ok(())
    }

    pub async fn get_logs(
        conn: &RqliteClient,
        image_id: &str,
    ) -> Result<Vec<LogEntry>, ModelError> {

        let get_image_q = ContainerImage::build_get_logs_query(image_id)?;
        let result = executors::fetch::<LogEntry>(conn, get_image_q).await?;

        Ok(result)

    }

    pub async fn from_id(
        conn: &RqliteClient,
        image_id: &str,
    ) -> Result<Option<Self>, ModelError> {

        let get_image_q = ContainerImage::build_from_id_query(image_id)?;
        let result = executors::fetch::<ContainerImage>(conn, get_image_q).await?;
        let container_image = result.first().cloned();

        if container_image.is_none() {
            return Ok(None)
        }
        let container_image = container_image.unwrap();

        Ok(Some(ContainerImage {
            id: container_image.id,
            name: container_image.name,
            tag: container_image.tag,
        }))

    }

    pub fn build_get_logs_query(
        image_id: &str,
    ) -> Result<RqliteQuery, QueryBuilderError> {

        let get_image_full_q = rqlite_rs::query!("select 
            image.id, 
            image.name, 
            image.tag, 
            logs.status, 
            logs.timestamp_utc, 
            logs.comment 
                from container_images as image 
                left join container_images_logs logs 
                on image.id = logs.id where image.id = ?", 
            image_id
        )?;

        Ok(get_image_full_q)

    }

    pub fn build_from_id_query(
        id: &str
    ) -> Result<RqliteQuery, QueryBuilderError> {
        
        let get_image_q = rqlite_rs::query!(
            "select * from container_images where id = ?",
            id
        )?;
        
        Ok(get_image_q)        

    }

    pub fn build_insert_requested_queries(
        id: &str,
        image_name: &str,
        image_tag: &str,
    ) -> Result<Vec<RqliteQuery>, QueryBuilderError> {

        // atomically register the image and set its status as "pulling"
        // note: "pulling" is the default status, so we don't need to insert anything more than the id
        let add_image_q = rqlite_rs::query!(
            "insert into container_images (id, name, tag) values(
                ?,
                ?,
                ?
            )", id, image_name, image_tag
        )?;

        let add_log_q = rqlite_rs::query!(
            "insert into container_images_logs (id) values(
                ?
            )", id
        )?;

        Ok(vec!(add_image_q, add_log_q))

    }
}

impl ContainerImages {

    pub async fn count(
        conn: &RqliteClient,
    ) -> Result<i64, ModelError> {

        let query = ContainerImages::build_count_query()?;
        let exec = executors::fetch::<ItemCount>(conn, query).await?;

        // this should always be valid, because it's a COUNT(*) query
        Ok(exec.first().unwrap().item_count)

    }

    pub fn build_count_query() -> Result<RqliteQuery, QueryBuilderError> {

        return Ok(
            rqlite_rs::query!("select count(*) as item_count from container_images")?
        )

    }

    pub async fn get_all(
        conn: &RqliteClient,
    ) -> Result<Vec<PaginatedContainerImage>, ModelError> {
        let all_images = ContainerImages::build_get_all_query()?;
        let result = executors::fetch::<PaginatedContainerImage>(conn, all_images).await?;
        Ok(result)
    }

    pub async fn paginate(
        conn: &RqliteClient,
        page: i64,
    ) -> Result<Vec<PaginatedContainerImage>, ModelError> {

        let paginate_q = ContainerImages::build_paginate_query(page)?;
        let result = executors::fetch::<PaginatedContainerImage>(conn, paginate_q).await?;

        Ok(result)

    }

    pub fn build_paginate_query(
        page: i64,
    ) -> Result<RqliteQuery, QueryBuilderError> {
        
        // page size: 20 rows
        // offset is calculated using the `page` variable as "index" and multiplying its
        // value by the page size. -1 is used because offset starts at 0, so the first page
        // is actually the 0-th offset

        // TODO: build a non-paginated version 

        // 17-12-2024: added status for api usability

        let get_images_paginate_q = rqlite_rs::query!(
            "select 
                ci.id,
                ci.name, 
                ci.tag, 
                (
                    select status from container_images_logs where id = ci.id and timestamp_utc >= (
                        select max(timestamp_utc) from container_images_logs where id = ci.id
                    )
                ) as status
                from container_images ci
            order by ci.name, ci.tag 
            limit 20 offset ?
        ", (page - 1) * 20)?; // there is no "all" operator in sqlite

        Ok(get_images_paginate_q)

    }

    pub fn build_get_all_query() -> Result<RqliteQuery, QueryBuilderError> {
        
        // "get-all" query 

        let get_images_q = rqlite_rs::query!(
            "select 
                ci.id,
                ci.name, 
                ci.tag, 
                (
                    select status from container_images_logs where id = ci.id and timestamp_utc >= (
                        select max(timestamp_utc) from container_images_logs where id = ci.id
                    )
                ) as status
                from container_images ci
            order by ci.name, ci.tag 
        ")?; // there is no "all" operator in sqlite

        Ok(get_images_q)

    }
}