use std::any;

use rqlite_rs::{error::QueryBuilderError, query::RqliteQuery, FromRow, IntoTypedRows, RqliteClient};
use serde::Serialize;
use thiserror::Error;
use utoipa::ToSchema;

use crate::{executors::{self, try_fetch_raw, ExecutorError}, ModelError};

#[derive(FromRow, Clone,)]
pub struct RawContainerImagesLogs {
    pub id: String,
    pub status: String,
    pub timestamp_utc: String,
    pub comment: Option<String>
}

#[derive(Serialize, ToSchema)]
pub struct ContainerImagesLogs {
    pub id: String,
    pub status: ContainerImageLogStatus,
    pub timestamp_utc: String,
    pub comment: Option<String>
}

impl From<ExecutorError> for ModelError{
    fn from(err: ExecutorError) -> Self {
        ModelError::RuntimeError(format!("executor error: {}", err.to_string()))
    }
}

impl From<QueryBuilderError> for ModelError{
    fn from(err: QueryBuilderError) -> Self {
        ModelError::RuntimeError(format!("query builder error: {}", err.to_string()))
    }
}

impl ContainerImagesLogs {

    pub async fn append_log_for_image(
        conn: &RqliteClient,
        id: &str,
        status: ContainerImageLogStatus,
        comment: Option<&str>
    ) -> Result<(), ModelError> {

        let query = ContainerImagesLogs::build_append_log_for_image_query(id, status, comment)?;
        executors::exec(conn, query).await?;

        Ok(())

    }

    pub fn build_append_log_for_image_query(
        id: &str,
        status: ContainerImageLogStatus,
        comment: Option<&str>
    ) -> Result<RqliteQuery, QueryBuilderError> {
        
        let append_log_q = match comment {
            Some(c) => {
                rqlite_rs::query!(
                    "insert into container_images_logs (id, status, comment) values (?, ?, ?)", 
                    id, status.to_string(), c.to_string()
                )?
            }
            None => {
                rqlite_rs::query!(
                    "insert into container_images_logs (id, status) values (?, ?)", 
                    id, status.to_string()
                )?
            }
        };

        Ok(append_log_q)

    }
}

#[derive(Clone, Serialize)]
pub enum ContainerImageLogStatus {
    Requested,
    Errored,
    Pulling,
    Done
}

impl TryInto<ContainerImagesLogs> for RawContainerImagesLogs {
    type Error = InvalidStatusError;
    fn try_into(self) -> Result<ContainerImagesLogs, Self::Error> {
        Ok(ContainerImagesLogs {
            status: self.status.try_into()?,
            id: self.id,
            timestamp_utc: self.timestamp_utc,
            comment: self.comment,
        })
    }
}

#[derive(Error, Debug)]
pub enum InvalidStatusError{
    #[error("unknown status: {0}")]
    UnknownStatus(String)
}

impl Into<String> for InvalidStatusError {
    fn into(self) -> String {
        match self {
            InvalidStatusError::UnknownStatus(s) => s,
        }
    }
}

impl TryInto<ContainerImageLogStatus> for String {
    type Error = InvalidStatusError;
    fn try_into(self) -> Result<ContainerImageLogStatus, Self::Error> {
        match self.as_str() {
            "R" => Ok(ContainerImageLogStatus::Requested),
            "E" => Ok(ContainerImageLogStatus::Errored),
            "P" => Ok(ContainerImageLogStatus::Pulling),
            "D" => Ok(ContainerImageLogStatus::Done),
            u => Err(InvalidStatusError::UnknownStatus(u.to_string()))
        }
    }
}

impl ToString for ContainerImageLogStatus {
    fn to_string(&self) -> String {
        match self {
            ContainerImageLogStatus::Requested => String::from("R"),
            ContainerImageLogStatus::Errored => String::from("E"),
            ContainerImageLogStatus::Pulling => String::from("P"),
            ContainerImageLogStatus::Done => String::from("D"),
        }
    }
}