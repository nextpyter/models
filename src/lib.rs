use rqlite_rs::FromRow;
use thiserror::Error;

pub mod container_images;
pub mod container_images_logs;
pub mod executors;

#[derive(Error, Debug)]
pub enum ModelError {
    #[error("error while executing: {0}")]
    RuntimeError(String)
}

#[derive(FromRow, Clone)]
pub struct ItemCount {
    pub item_count: i64,
}