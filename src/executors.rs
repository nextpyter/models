use rqlite_rs::{error::RequestError, query::RqliteQuery, query_result::QueryResult, response::{QueryError, RqliteResult}, FromRow, IntoTypedError, IntoTypedRows, RqliteClient};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ExecutorError{
    #[error("couldn't perform into_typed: {0}")]
    CannotIntoTyped(String),
    #[error("rqlite request error: {0}")]
    RqliteRequestError(String),
    #[error("collection error: {0}")]
    CannotCollectError(String),
}

impl From<IntoTypedError> for ExecutorError {
    fn from(value: IntoTypedError) -> Self {
        ExecutorError::CannotIntoTyped(value.to_string())
    }
}

impl From<RequestError> for ExecutorError {
    fn from(value: RequestError) -> Self {
        ExecutorError::RqliteRequestError(value.to_string())
    }
}

/// this function is used when the type (Raw) that gets returned from the database
/// cannot implement FromRow and the returned type (Cooked) can "throw" when being
/// converted from Raw
pub async fn try_fetch_raw<Raw, Cooked>(
    conn: &RqliteClient,
    query: RqliteQuery,
) -> Result<Vec<Cooked>, ExecutorError> 
    where 
        Raw: FromRow + Clone + TryInto<Cooked>, // we need these traits because otherwise we wouldn't be able to type the row
        <Raw as TryInto<Cooked>>::Error: Into<String>, // the error must be stringifiable
{
    let fetch = conn.fetch(query).await?;
    let raw_res = fetch.into_typed::<Raw>()?;
    match raw_res
        .into_iter()
        .map(|i| i.try_into())
        .collect::<Result<Vec<Cooked>, _>>() {
            Ok(result) => Ok(result),
            Err(e) => Err(ExecutorError::CannotCollectError(e.into()))
        }

}

/// this function is the same as `try_fetch_raw` but Cooked can always be converted
/// from Raw
pub async fn fetch_raw<Raw, Cooked>(
    conn: &RqliteClient,
    query: RqliteQuery,
) -> Result<Vec<Cooked>, ExecutorError> 
    where 
        Raw: FromRow + Clone + Into<Cooked>, // we need these traits because otherwise we wouldn't be able to type the row
{
    let fetch = conn.fetch(query).await?;
    let raw_res = fetch.into_typed::<Raw>()?;
    
    Ok(raw_res
        .into_iter()
        .map(|i| i.into())
        .collect::<Vec<Cooked>>()
    )

}

/// this function is used when T directly implements FromRow, thus being
/// directly serializable
pub async fn fetch<T>(
    conn: &RqliteClient,
    query: RqliteQuery,
) -> Result<Vec<T>, ExecutorError> 
    where 
        T: FromRow + Clone, // we need these traits because otherwise we wouldn't be able to type the row
{
    let fetch = conn.fetch(query).await?;
    let raw_res = fetch.into_typed::<T>()?;
    
    Ok(raw_res)

}

/// this function simply executes a single query
pub async fn exec(
    conn: &RqliteClient,
    query: RqliteQuery
) -> Result<(), ExecutorError> {
    conn.exec(query).await?;
    Ok(())
}

/// this function executes a Vec of queries atomically (inside a transaction)
pub async fn transaction(
    conn: &RqliteClient,
    queries: Vec<RqliteQuery>
) -> Result<(), ExecutorError> {
    let result = conn.transaction(queries).await?.into_iter();
    let mut errors = result.filter(|item|{
        match item {
            rqlite_rs::response::RqliteResult::Error(e) => true,
            rqlite_rs::response::RqliteResult::Success(_) => false,
        }
    });

    if let Some(RqliteResult::Error(err)) = errors.next() {
        return Err(ExecutorError::RqliteRequestError(err.error))
    }

    Ok(())
}